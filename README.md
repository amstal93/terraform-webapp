

![logo][logo]



[![pipeline status](https://gitlab.com/skalesys/terraform-webapp/badges/master/pipeline.svg)](https://gitlab.com/skalesys/terraform-webapp/commits/master)



 
# terraform-webapp 


 
Terraform workspace that implements a secure web application infrastructure. 


---


## Screenshots


![cloudfront-distribution](docs/screenshots/cloudfront-distribution.png) |
|:--:|
| *Cloudfront distribution settings* |


![cloudfront-dist-general](docs/screenshots/cloudfront-dist-general.png) |
|:--:|
| *Cloudfront dist general settings* |


![cloudfront-dist-origin](docs/screenshots/cloudfront-dist-origin.png) |
|:--:|
| *Cloudfront dist origin settings* |


![cloudfront-dist-behaviors](docs/screenshots/cloudfront-dist-behaviors.png) |
|:--:|
| *Cloudfront dist behaviors settings* |


![cloudfront-dist-tags](docs/screenshots/cloudfront-dist-tags.png) |
|:--:|
| *Cloudfront dist tags settings* |


![acm-certificate](docs/screenshots/acm-certificate.png) |
|:--:|
| *ACM certificate* |


![rds-database](docs/screenshots/rds-database.png) |
|:--:|
| *RDS database* |


![rds-database-configuration](docs/screenshots/rds-database-configuration.png) |
|:--:|
| *RDS database configuration settings* |


![rds-database-connectivity-security](docs/screenshots/rds-database-connectivity-security.png) |
|:--:|
| *RDS database connectivity-security settings* |


![rds-maintenance](docs/screenshots/rds-maintenance.png) |
|:--:|
| *RDS maintenance settings* |


![rds-database-tags](docs/screenshots/rds-database-tags.png) |
|:--:|
| *RDS database tags settings* |









## Requirements

In order to run this project you will need: 

- [Ubuntu 18.04][ubuntu] - Linux operating system
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code




## Usage

First you must to create the certificate on [AWS Certificate Manager][acm]:
```
make certificate/new

{
    "CertificateArn": "arn:aws:acm:us-east-1:096373988534:certificate/90248292-9253-4999-8f07-260d6a174946"
}

```

Now you can plan and apply the changes using [Terraform][terraform]:
```bash
make init install
make terraform/plan STAGE=development TF_VAR_db_admin_password=ipxFDBsPT9i8AbzN
make terraform/apply STAGE=development
```

Destroy all resources:
```
make terraform/destroy STAGE=development TF_VAR_db_admin_password=ipxFDBsPT9i8AbzN
```

How to SSH into a private instance using SSH:
```
bastion_ip=52.64.145.229
private_instance_ip=10.1.2.120
ssh -i secrets/sk-dev-webapp.pem -Ao ProxyCommand="ssh -i secrets/sk-dev-webapp.pem -W %h:%p -p 22 ubuntu@$bastion_ip" -p 22 ubuntu@$private_instance_ip
```
This is just an example, you can get more sophisticated solutions involving with `~/.ssh/config` or `alias`.

How to connect to the dabase:
```
# first you need to jump to one of the private instances, as described above
# You need to install mysql-client:
sudo apt-get install --yes mysql-client

# Now you can connect to the database, you need to provide the same password as `TF_VAR_db_admin_password`
mysql -h master.webapp.dev.skalesys.com -u admin -p

*`TF_VAR_db_admin_password`* value is just an example.







## Makefile targets

```Available targets:

  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  certificate/global                 	Use the AWS cli to request new ACM certifiates available globally (us-east-1) (requires email validation, check SPAM folder)
  certificate/regional               	Use the AWS cli to request new ACM certifiates available at $(REGION) (requires email validation, check SPAM folder)
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  install                            	Install project requirements
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```








## References

For additional context, refer to some of these links. 

- [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) - AWS Certificate Manager is a service that lets you easily provision, manage, and deploy public and private Secure Sockets Layer/Transport Layer Security (SSL/TLS) certificates for use with AWS services and your internal connected resources.
- [Amazon Cloudfront](https://aws.amazon.com/cloudfront/) - Amazon CloudFront is a fast content delivery network (CDN) service that securely delivers data, videos, applications, and APIs to customers globally with low latency, high transfer speeds, all within a developer-friendly environment. 




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/m_HRfLhgABo) - Photo by Christopher Gower on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/






[branches]: https://gitlab.com/skalesys/terraform-webapp/branches
[commits]: https://gitlab.com/skalesys/terraform-webapp/commits
[tags]: https://gitlab.com/skalesys/terraform-webapp/tags
[contributors]: https://gitlab.com/skalesys/terraform-webapp/graphs
[graph]: https://gitlab.com/skalesys/terraform-webapp/network
[charts]: https://gitlab.com/skalesys/terraform-webapp/charts


